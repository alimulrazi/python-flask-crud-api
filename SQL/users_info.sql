/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.27-log 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `users_info` (
	`id` int (11),
	`name` varchar (765),
	`email` varchar (765),
	`phone` varchar (48),
	`address` text 
); 
insert into `users_info` (`id`, `name`, `email`, `phone`, `address`) values('2','Smith','smith@webdamn.com','1234567890','Newyork, USA');
insert into `users_info` (`id`, `name`, `email`, `phone`, `address`) values('3','Flower','flower@webdamn.com','1234567890','Paris');
